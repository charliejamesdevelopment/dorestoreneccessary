var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var configAuth = require('./keys');

module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
      done(null, user);
    });

    passport.deserializeUser(function(user, done) {
      done(null, user);
    });

    console.log(configAuth.googleAuth.callbackURL)

    passport.use(new GoogleStrategy({
        clientID: configAuth.googleAuth.clientID,
        clientSecret: configAuth.googleAuth.clientSecret,
        callbackURL: configAuth.googleAuth.callbackURL
    }, function(token, refreshToken, profile, done) {
        process.nextTick(function() {
          done(null, profile)
        });
    }));

};
