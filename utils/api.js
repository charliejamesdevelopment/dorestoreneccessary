module.exports = {
  checkAuthentication: function(req, cb) {
    if(req.session && req.session.passport && req.session.passport.user) {
      cb(true, req.session.passport.user)
    }

    cb(false, null)
  }
}
